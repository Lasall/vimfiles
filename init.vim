set number
syntax on

set mouse=a
"set termguicolors

set tabstop=4      " number of visual spaces per \t
set shiftwidth=4
set softtabstop=4  " number of spaces in tab
set expandtab      " tabs are spaces

set nowrap
set linebreak

set listchars=extends:›,precedes:‹,nbsp:␣,trail:·,tab:▸\  
set list
set showbreak=↪\ 
"Invisible character colors
highlight SpecialKey ctermfg=5
highlight NonText guifg=#4a4a59
highlight SpecialKey guifg=#4a4a59
" set showbreak=
highlight EOLWhitespace ctermbg=darkgray
match EOLWhitespace /\s\+$/

inoremap <C-U> <C-G>u<C-U>
set undofile
set undodir=~/.config/nvim/undo//

set directory=~/.config/nvim/swapfiles//
set backupdir=~/.config/nvim/backups//

" allow the . to execute once for each line of a visual selection
vnoremap . :normal .<CR>

" show current line
set cursorline

" load filetype specific indent files
filetype indent on
filetype plugin on

" visual autocomplete for command menu
set wildmenu

" only draw when necessary
"set lazyredraw

" highlight matching paranthesis
set showmatch

" Allow the cursor to go in to "invalid" places
set virtualedit=all

" When completing by tag, show the whole tag, not just the function name
set showfulltag

" search stuff
set incsearch
set hlsearch
set ignorecase
set smartcase

" Make command line two lines high
set cmdheight=1

" leader is comma
let mapleader=","
" local leader is single backslash
let maplocalleader="\\"

" stop search with ,n
nnoremap <leader>n :nohlsearch<CR>

" enable folding
set foldenable
set foldlevelstart=10
set foldmethod=syntax

" dispaly line numbers
set number

" colorscheme xoria256
colo xoria256

" remember last position
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif

" working with tabs
map <F5> :tabe <cr>
map <F6> :tabp<cr>
map <F7> :tabn<cr>

" close quickfix list
nnoremap <leader>cl    :ccl<CR>
" open quickfix list
nnoremap <leader>co    :botright copen<CR>
" next quickfix item
nnoremap <leader>cn    :cnext<CR>
" previous quickfix item
nnoremap <leader>cp    :cprevious<CR>
" previous quickfix item
nnoremap <leader>cf    :cnfile<CR>

" never hide anything
"set conceallevel=0    " <- indentLine plugin will set this to > 1 to work

" set foldcolumn=4
set showcmd

set scrolloff=10

set pastetoggle=<leader>p

" You are too fast and keep pressing `shift' if you type :w, try following
":command! -bang W w<bang>
command! -bang -bar -nargs=? -complete=file -range=% W <line1>,<line2>w<bang> <args>
command! -bang Wq wq<bang>
command! -bang Wall wall<bang>
command! -bang Vsp vsp<bang>
command! -bang Sp sp<bang>
command! -bang Q q<bang>
command! -bang Qall qall<bang>

" faster movement between panes
nnoremap <C-l> <C-w>l
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k

au TermOpen * tnoremap <buffer> <Esc> <c-\><c-n>

" move between displayed lines/columns always
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk
nnoremap  1 g0
nnoremap  4 g$
vnoremap  1 g0
vnoremap  4 g$
nnoremap <Down> gj
nnoremap <Up> gk
vnoremap <Down> gj
vnoremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

" show partially hidden wrapped lines
set display+=lastline
set wrap

" don't save options to session file, so vimrc/plugin changes are acknowledged
set sessionoptions-=options

" Update syntax highlighting
noremap <F8> <Esc>:syntax sync fromstart<CR>
inoremap <F8> <C-o>:syntax sync fromstart<CR>


" ____________________________________________________________________________
"
" plugins
"
call plug#begin('~/.config/nvim/vim-plug')
Plug 'scrooloose/nerdtree'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-airline/vim-airline'
Plug 'jackguo380/vim-lsp-cxx-highlight'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'junegunn/fzf', { 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tomtom/tcomment_vim'
Plug 'majutsushi/tagbar'
Plug 'peterhoeg/vim-qml'
Plug 'mbbill/undotree'
Plug 'junegunn/vim-peekaboo'
Plug 'lervag/vimtex'
Plug 'jiangmiao/auto-pairs'
Plug 'Yggdroot/indentLine'
Plug 'tenfyzhong/CompleteParameter.vim'
Plug 'junegunn/vader.vim'
Plug 'moll/vim-bbye'
"Plug 'cespare/vim-toml'
Plug 'dahu/vim-fanfingtastic'
Plug 'aklt/plantuml-syntax'
Plug 'artoj/qmake-syntax-vim'
Plug 'udalov/kotlin-vim'
" Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" Plug 'floobits/floobits-neovim'
" Plug 'file:///home/dominique/projects/yammap'
" Plug 'severin-lemaignan/vim-minimap'
" Plug 'vifm/vifm.vim'
" Plug 'airodactyl/neovim-ranger'
" Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'mcchrish/nnn.vim'
" Plug 'lambdalisue/fern.vim'
" Plug 'tpope/vim-vinegar'
" Plug 'fenetikm/falcon'
" Plug 'vim-airline/vim-airline-themes'
call plug#end()

"set rtp+=~/projects/yammap


" vim-go use gopls as lsp
let g:go_def_mode='~/go/bin/gopls'
let g:go_info_mode='~/go/bin/gopls'

" detect hugo templates
function DetectGoHtmlTmpl()
    if expand('%:e') == "html" && search("{{") != 0
        set filetype=gohtmltmpl 
    endif
endfunction

augroup filetypedetect
    au BufRead,BufNewFile * call DetectGoHtmlTmpl()
    au! BufRead,BufNewFile *.plist set ft=xml
augroup END


" Fanfing Tastig
" use f F t T ; , across lines
let g:fanfingtastic_ignorecase = 1

" indentLine needs to set conceallevel > 0 unfortunately
let g:indentLine_setConceal = 1
let g:indentLine_fileTypeExclude = ['markdown', 'tex', 'latex']

" Bbye setting, close current buffer
:nnoremap <Leader>q :Bdelete<CR>

" coc-clangd
nnoremap <leader>of :CocCommand clangd.switchSourceHeader<CR>

" auto-pairs compatibility for CompleteParameter
let g:AutoPairs = {'[':']', '{':'}',"'":"'",'"':'"', '`':'`'}
"inoremap <buffer><silent> ) <C-R>=AutoPairsInsert(')')<CR>
"inoremap <buffer><silent> > <C-R>=AutoPairsInsert('>')<CR>

" CompleteParameter
"let g:complete_parameter_log_level = 1
" inoremap <silent><expr> ( complete_parameter#pre_complete("()")
" inoremap <silent><expr> < complete_parameter#pre_complete("<>")
smap <m-j> <Plug>(complete_parameter#goto_next_parameter)
imap <m-j> <Plug>(complete_parameter#goto_next_parameter)
smap <m-k> <Plug>(complete_parameter#goto_previous_parameter)
imap <m-k> <Plug>(complete_parameter#goto_previous_parameter)
nmap <m-d> <Plug>(complete_parameter#overload_down)
imap <m-d> <Plug>(complete_parameter#overload_down)
smap <m-d> <Plug>(complete_parameter#overload_down)
nmap <m-u> <Plug>(complete_parameter#overload_up)
imap <m-u> <Plug>(complete_parameter#overload_up)
smap <m-u> <Plug>(complete_parameter#overload_up)

" vimtex
let g:tex_flavor = 'latex'
let g:vimtex_compiler_progname = 'nvr'
let maplocalleader = "m"
let g:vimtex_compiler_latexmk_engines = {
        \ '_'                : '-pdflua',
        \ 'pdflatex'         : '-pdf',
        \ 'dvipdfex'         : '-pdfdvi',
        \ 'lualatex'         : '-lualatex',
        \ 'xelatex'          : '-xelatex',
        \ 'context (pdftex)' : '-pdf -pdflatex=texexec',
        \ 'context (luatex)' : '-pdf -pdflatex=context',
        \ 'context (xetex)'  : '-pdf -pdflatex=''texexec --xtx''',
        \}
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
"let g:vimtex_view_general_options_latexmk = '--unique'
" let g:vimtex_quickfix_warnings = {
"   \ 'default' : 0,
"   \ 'undefined_reference' : 1,
"   \ 'multiply_defined_references' : 1,
"   \ 'packages' : {
"   \   'default' : 0,
"   \ },
"   \}
" let g:vimtex_quickfix_ignore_filters = [
"           \ 'float specifier changed to',
"           \]
let g:vimtex_quickfix_open_on_warning = 0
"let g:tex_conceal = ''

" qml folding
let g:qml_fold = 1

" undotree
nmap <F2> :UndotreeToggle<CR>

" " nerdtree
nmap <F3> :NERDTreeToggle<CR>

" coc-explorer
" nmap <F3> :CocCommand explorer<CR>

" tagbar
nmap <F4> :TagbarToggle<CR>

let g:tagbar_type_tex = {
    \ 'ctagstype' : 'latex',
    \ 'kinds'     : [
        \ 's:sections',
        \ 'g:graphics:0:0',
        \ 'l:labels',
        \ 'r:refs:1:0',
        \ 'p:pagerefs:1:0'
    \ ],
    \ 'sort'    : 0,
\ }

" FZF
" Use enter key, CTRL-T, CTRL-X or CTRL-V to open selected files in the
" current window, in new tabs, in horizontal splits, or in vertical splits
" respectively
au FileType fzf tunmap <buffer> <Esc>
nnoremap <silent> <leader>b :Buffers<cr>
nnoremap <silent> <leader>t :Files<cr>
nnoremap <silent> <leader>s :Lines<cr>
"nnoremap <silent> <leader>h :FZF ~<cr>


" airline powerline fonts
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#hunks#enabled = 0


" install coc extensions
let g:coc_global_extensions = [
    \  "coc-vimtex"
    \, "coc-highlight"
    \, "coc-git"
    \, "coc-clangd"
    \, "coc-cmake"
    \, "coc-docker"
    \, "coc-groovy"
    \, "coc-toml"
    \, "coc-pyright"
    \, "coc-snippets"
    \, "coc-json"
    \, "coc-tsserver"
    \, "coc-markdownlint"
    \, "coc-vimlsp"
    \, "coc-yaml"
    \, "coc-svg"
    \, "coc-html"
    \, "coc-yank"
    \, "coc-css"
    \, "coc-actions"
    \, "coc-java"
    \, "coc-lua"
    \, "coc-sh"
    \, "coc-xml"
    \, "coc-kotlin"
    \, "coc-spell-checker"
\ ]


" coc-actions


" coc-completion
" Remap for do codeAction of selected region
"function! s:cocActionsOpenFromSelected(type) abort
"  execute 'CocCommand actions.open ' . a:type
"endfunction
"xmap <silent> <leader>a :<C-u>execute 'CocCommand actions.open ' . visualmode()<CR>
"nmap <silent> <leader>a :<C-u>set operatorfunc=<SID>cocActionsOpenFromSelected<CR>g@

 " Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'
" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'


" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
"set nobackup
"set nowritebackup

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience. Needs to be low for highlight to work
set updatetime=200

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=auto:2

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <leader>gdc <Plug>(coc-declaration)
nmap <leader>gdf <Plug>(coc-definition)
nmap <leader>gt <Plug>(coc-type-definition)
nmap <leader>go <Plug>(coc-implementation)
nmap <leader>gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor. Needs low
" updatetime.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s)
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
"nmap <leader>ac  <Plug>(coc-codeaction)
" Remap keys for apply code actions affect whole buffer
"nmap <leader>as  <Plug>(coc-codeaction-source)
" Apply AutoFix to problem on the current line.
nnoremap <leader>qf  <Plug>(coc-fix-current)

" Remap keys for applying refactor code actions
"nmap <silent> <leader>re <Plug>(coc-codeaction-refactor)
"xmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)
"nmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server
"xmap if <Plug>(coc-funcobj-i)
"omap if <Plug>(coc-funcobj-i)
"xmap af <Plug>(coc-funcobj-a)
"omap af <Plug>(coc-funcobj-a)
"xmap ic <Plug>(coc-classobj-i)
"omap ic <Plug>(coc-classobj-i)
"xmap ac <Plug>(coc-classobj-a)
"omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> to scroll float windows/popups
nnoremap <silent><nowait><expr> <C-j> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-j>"
nnoremap <silent><nowait><expr> <C-k> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-k>"
inoremap <silent><nowait><expr> <C-j> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <silent><nowait><expr> <C-k> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
vnoremap <silent><nowait><expr> <C-j> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-j>"
vnoremap <silent><nowait><expr> <C-k> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-k>"

" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
"nmap <silent> <TAB> <Plug>(coc-range-select)
"xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
"set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" coc-yank
nnoremap <silent> <space>y  :<C-u>CocList --normal yank<cr>
